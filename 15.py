inp = """Sensor at x=2885528, y=2847539: closest beacon is at x=2966570, y=2470834
Sensor at x=2224704, y=1992385: closest beacon is at x=2018927, y=2000000
Sensor at x=3829144, y=1633329: closest beacon is at x=2966570, y=2470834
Sensor at x=43913, y=426799: closest beacon is at x=152363, y=369618
Sensor at x=2257417, y=2118161: closest beacon is at x=2386559, y=2090397
Sensor at x=8318, y=3994839: closest beacon is at x=-266803, y=2440278
Sensor at x=69961, y=586273: closest beacon is at x=152363, y=369618
Sensor at x=3931562, y=3361721: closest beacon is at x=3580400, y=3200980
Sensor at x=476279, y=3079924: closest beacon is at x=-266803, y=2440278
Sensor at x=2719185, y=2361091: closest beacon is at x=2966570, y=2470834
Sensor at x=2533382, y=3320911: closest beacon is at x=2260632, y=3415930
Sensor at x=3112735, y=3334946: closest beacon is at x=3580400, y=3200980
Sensor at x=1842258, y=3998928: closest beacon is at x=2260632, y=3415930
Sensor at x=3712771, y=3760832: closest beacon is at x=3580400, y=3200980
Sensor at x=1500246, y=2684955: closest beacon is at x=2018927, y=2000000
Sensor at x=3589321, y=142859: closest beacon is at x=4547643, y=-589891
Sensor at x=1754684, y=2330721: closest beacon is at x=2018927, y=2000000
Sensor at x=2476631, y=3679883: closest beacon is at x=2260632, y=3415930
Sensor at x=27333, y=274008: closest beacon is at x=152363, y=369618
Sensor at x=158732, y=2405833: closest beacon is at x=-266803, y=2440278
Sensor at x=2955669, y=3976939: closest beacon is at x=3035522, y=4959118
Sensor at x=1744196, y=13645: closest beacon is at x=152363, y=369618
Sensor at x=981165, y=1363480: closest beacon is at x=2018927, y=2000000
Sensor at x=2612279, y=2151377: closest beacon is at x=2386559, y=2090397
Sensor at x=3897, y=2076376: closest beacon is at x=-266803, y=2440278
Sensor at x=2108479, y=1928318: closest beacon is at x=2018927, y=2000000
Sensor at x=1913043, y=3017841: closest beacon is at x=2260632, y=3415930
Sensor at x=2446778, y=785075: closest beacon is at x=2386559, y=2090397
Sensor at x=2385258, y=2774943: closest beacon is at x=2386559, y=2090397
Sensor at x=3337656, y=2916144: closest beacon is at x=3580400, y=3200980
Sensor at x=380595, y=66906: closest beacon is at x=152363, y=369618
Sensor at x=1593628, y=3408455: closest beacon is at x=2260632, y=3415930"""

inp2 = """Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3"""

#inp = inp2

def parse_line(line):
	pos, beacon = line.split(": closest beacon is at x=")
	pos = pos[12:]
	pos = list(int(el) for el in pos.split(", y="))
	beacon = list(int(el) for el in beacon.split(", y="))
	dist = abs(pos[0] - beacon[0]) + abs(pos[1] - beacon[1])
	return pos, beacon, dist

converted = list(parse_line(el) for el in inp.split("\n"))
#print(converted)

ypos = 2000000
positions = set()
for pos, beacon, dist in converted:
	possibilities = -(abs(pos[1] - ypos) - dist)
	#print(pos, beacon, dist, possibilities)
	if possibilities < 0:
		continue
	to_add = set(range(pos[0] - possibilities, pos[0] + possibilities + 1))
	if beacon[1] == ypos:
		to_add -= {beacon[0]}
	positions.update(to_add)

print(len(positions))

maxval = 4000000
#maxval = 20; inp = inp2

def intersect(orig, new, minval, maxval):
	result = list()
	for tpl in orig:
		if tpl[1] < new[0] or tpl[0] > new[1]:
			result += [tpl]
			continue
		elif tpl[0] >= new[0] and tpl[1] <= new[1]:
			#print("erased", tpl, "with", new)
			continue # completely gone
		elif tpl[0] < new[0] and tpl[1] > new[1]:
			result.append((tpl[0], new[0] - 1))
			result.append((new[1] + 1, tpl[1]))
			continue
		elif tpl[0] >= new[0]:
			result.append((max(minval, new[1] + 1), tpl[1]))
			continue
		else:
			result.append((tpl[0], min(new[0] - 1, maxval)))
			continue
	#print(orig, new, "=>", result)
	return result

def print_search_space(search_space):
	print(search_space)
	for line in search_space:
		res = ["#" for i in range(maxval + 1)]
		for tpl in line:
			for i in range(tpl[0], tpl[1] + 1):
				res[i] = "."
		print(res)



converted = list(parse_line(el) for el in inp.split("\n"))
search_space = list([(0, maxval)] for i in range(maxval + 1))
#print_search_space(search_space)
for pos, beacon, dist in converted:
	for ypos in range(len(search_space)):
		possibilities = -(abs(pos[1] - ypos) - dist)
		#print(pos, beacon, dist, possibilities)
		if possibilities < 0:
			continue
		search_space[ypos] = intersect(search_space[ypos], (pos[0] - possibilities, pos[0] + possibilities), 0, maxval)
	#print_search_space(search_space)
	print("progress")

allvals = list(filter(lambda el: el[1] != [], enumerate(search_space)))
if len(allvals) > 1:
	print("helpmepls")
	exit(1)
i, result = allvals[0]
print(i, result)
print(result[0][0] * 4000000 + i)


