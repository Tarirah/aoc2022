inp = """Monkey 0:
  Starting items: 78, 53, 89, 51, 52, 59, 58, 85
  Operation: new = old * 3
  Test: divisible by 5
    If true: throw to monkey 2
    If false: throw to monkey 7

Monkey 1:
  Starting items: 64
  Operation: new = old + 7
  Test: divisible by 2
    If true: throw to monkey 3
    If false: throw to monkey 6

Monkey 2:
  Starting items: 71, 93, 65, 82
  Operation: new = old + 5
  Test: divisible by 13
    If true: throw to monkey 5
    If false: throw to monkey 4

Monkey 3:
  Starting items: 67, 73, 95, 75, 56, 74
  Operation: new = old + 8
  Test: divisible by 19
    If true: throw to monkey 6
    If false: throw to monkey 0

Monkey 4:
  Starting items: 85, 91, 90
  Operation: new = old + 4
  Test: divisible by 11
    If true: throw to monkey 3
    If false: throw to monkey 1

Monkey 5:
  Starting items: 67, 96, 69, 55, 70, 83, 62
  Operation: new = old * 2
  Test: divisible by 3
    If true: throw to monkey 4
    If false: throw to monkey 1

Monkey 6:
  Starting items: 53, 86, 98, 70, 64
  Operation: new = old + 6
  Test: divisible by 7
    If true: throw to monkey 7
    If false: throw to monkey 0

Monkey 7:
  Starting items: 88, 64
  Operation: new = old * old
  Test: divisible by 17
    If true: throw to monkey 2
    If false: throw to monkey 5"""

inp2 = """Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1"""

#inp = inp2

inp = inp.replace("Test: divisible by", "Test:\n    divisible by:")
inp = inp.replace(": throw to monkey ", ":\n      throw to monkey: ")
print(inp)

import yaml
inp = yaml.safe_load(inp)

class Monkey(object):
	def make_operation(self):
		self.operation = self.yaml["Operation"]
		self.operation = self.operation.split(" = ")[1]
		fst, op, scn = self.operation.split(" ")
		if op == "+":
			op = lambda x, y: x+y 
		else:
			op = lambda x, y: x*y
		def operation(val):
			if fst == "old":
				x = val
			else:
				x = int(fst)
			if scn == "old":
				y = val
			else:
				y = int(scn)
			#print("op:", x, y, op(x,y))
			return op(x,y)
		self.op = operation
	def make_test(self):
		self.test = self.yaml["Test"]
		self.divisor = self.test["divisible by"]
		self.true_target = self.test["If true"]["throw to monkey"]
		self.false_target = self.test["If false"]["throw to monkey"]
		def target(val):
			if val % self.divisor == 0:
				return self.true_target
			else:
				return self.false_target
		self.target = target

	def make_next(self):
		def next(val):
			val = self.op(val)
			#print("val:",val)
			val = val // 3
			self.monkeys[self.target(val)].items.append(val)
			self.counter += 1
		self.next = next

	def make_part2_next(self):
		def next(vals):
			vals = [self.op(el) % divisor for el, divisor in zip(vals, self.divisors)]
			self.monkeys[self.target(vals[self.index])].pt2_items.append(vals)
			self.pt2_counter += 1
		self.pt2_next = next

	def __init__(self, yaml, index, num_monkeys):
		self.monkeys = list()
		self.counter = 0
		self.pt2_counter = 0
		self.index = index

		self.yaml = yaml
		self.items = yaml["Starting items"]
		self.items = [int(el) for el in self.items.split(", ")] if type(self.items) is str else [self.items]
		self.pt2_items = [[el] * num_monkeys for el in self.items]
		self.make_operation()
		self.make_test()
		self.make_next()
		self.make_part2_next()

monkeys = list()
num_monkeys = len(inp)
for i, (_, val) in enumerate(inp.items()):
	monkeys.append(Monkey(val, i, num_monkeys))

divisors = [m.divisor for m in monkeys]
for m in monkeys:
	m.monkeys = monkeys
	m.divisors = divisors

for i in range(20):
	for j, m in enumerate(monkeys):
		for el in m.items:
			m.next(el)
		m.items = list()

items = [m.counter for m in monkeys]
print(items)
items.sort()
print(items[-2] * items[-1])

for i in range(10000):
	for j, m in enumerate(monkeys):
		for el in m.pt2_items:
			m.pt2_next(el)
		m.pt2_items = list()
items = [m.pt2_counter for m in monkeys]
print(items)
items.sort()
print(items[-2] * items[-1])
